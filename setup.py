#!/usr/bin/python

from setuptools import setup, find_packages

# to set __version__
exec(open('gitsec/version.py').read())

setup(
    name='gitsec',
    version=__version__,
    url='https://github.com/blockstack/gitsec',
    license='GPLv3',
    author='Blockstack.org',
    author_email='support@blockstack.org',
    description='Tool for using Blockstack DNS to sign and verify Git commits',
    keywords='blockchain git crypography name key value store data',
    packages=find_packages(),
    scripts=['bin/gitsec'],
    download_url='https://github.com/blockstack/gitsec/archive/master.zip',
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        'blockstack-client>=0.0.12.5'
    ],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet',
        'Topic :: Security :: Cryptography',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
