#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    gitsec
    ~~~~~
    copyright: (c) 2014-2015 by Halfmoon Labs, Inc.
    copyright: (c) 2016 by Blockstack.org

    This file is part of gitsec.

    Gitsec is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gitsec is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with gitsec.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import subprocess

from paths import *
from db import *
from gpg import *

def git_run( args, git_path=None, stdin=None, basepath=None ):
    """
    Run git with the given args.
    Return (exitcode, stdout, stderr)
    """
    if git_path is None:
        git_path = "git"

    if basepath is None:
        basepath = gitsec_path()

    git_env = os.environ.copy()
    git_env["GITSEC_GPG_HOME"] = basepath
    
    popen_stdin = None 
    if stdin is not None:
        popen_stdin = subprocess.PIPE

    git = subprocess.Popen( [git_path] + [str(a) for a in args], shell=False, stdin=popen_stdin, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=git_env )
    git_out, git_err = git.communicate( input=stdin )
    git.wait()

    return (git.returncode, git_out, git_err)


def consume_log_entry( log_tokens ):
    """
    Parse the output of a specially-crafted log token stream.
    Use with --pretty="format:%H\n%G?\ni%GK\n%an\n%ae\n%aD\n%cn\n%ce\n%cD\n%s\nEOL""
    Return a dict of:
        * commit_hash: str
        * sig_status: str
        * signing_key: str
        * author_name: str
        * author_email: str
        * author_date: str
        * committer_name: str
        * committer_email: str
        * committer_date: str
        * message: str

    Raise ValueError if we couldn't find what we're looking for
    """

    token_order = [
        "commit_hash",
        "sig_status",
        "signing_key",
        "author_name",
        "author_email",
        "author_date",
        "committer_name",
        "committer_email",
        "committer_date",
        "message"
    ]
    
    ret = {}

    if len(log_tokens) < len(token_order) + 1:
        raise ValueError("Not enough tokens")

    # stream must end with "EOL"
    if log_tokens[ len(token_order) ] != "EOL":
        raise ValueError("Corrupt token stream (no EOL found)")

    for i in xrange(0, token_order):
        token = log_tokens.pop(0)
        ret[ token_order[i] ] = token

    # get rid of EOL
    log_tokens.pop(0)

    return ret


def git_log( repo_path=None, basepath=None, depth=None, file_path=None ):
    """
    Find commits that are not signed in the given repo
    Use the keys in the current .gitsec data.
    Return a list of unsigned log entries, as a list of dicts (see consume_log_entry)
    Raise if git fails.
    """
    args = ['-c', 'gpg.program=gitsec-gpg', 'log', '--pretty="format:%H\n%G?\n%GK\n%an\n%ae\n%aD\n%cn\n%ce\n%cD\n%s\nEOL"']

    if depth is not None:
        # only go back so far
        args.append( "-%s" % depth )

    if file_path is not None:
        # check log on an individual file, not the entire repo 
        args += ['--', file_path]

    if repo_path is not None:
        # cd to this directory first
        args = ['-C', repo_path] + args

    exitcode, stdout, stderr = git_run( args, basepath=basepath )
    if exitcode != 0:
        log.error("git error:\n\n%s\n" % stderr)
        raise Exception("git exit code %s" % exitcode)

    # parse stdout
    log_tokens = stdout.split("\n")
    log_entries = []
    while len(log_tokens) > 0:
        log_entry = consume_log_entry( log_tokens )
        log_entries.append( log_entry )

    return log_entries


