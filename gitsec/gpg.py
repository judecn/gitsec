#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    gitsec
    ~~~~~
    copyright: (c) 2014-2015 by Halfmoon Labs, Inc.
    copyright: (c) 2016 by Blockstack.org

    This file is part of gitsec.

    Gitsec is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gitsec is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with gitsec.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import traceback
import gnupg
import urllib2
import tempfile
import shutil
import base64
import copy
import json
from ConfigParser import SafeConfigParser

from log import log 
from paths import *

def make_gpg_home(basepath=None):
    """
    Make gitsec keyring dir.
    Return the path.
    """

    path = gitsec_keydir( basepath )

    if not os.path.exists(path):
        os.makedirs( path, 0700 )
    else:
        os.chmod( path, 0700 )

    return path


def gpg_store_key( key_bin, gitsec_dir=None ):
    """
    Store a public key to our gitsec keyring.
    Return the key ID on success
    Return None on error
    """

    if gitsec_dir is None:
        gitsec_dir = gitsec_path()

    keydir = make_gpg_home( gitsec_dir )
    gpg = gnupg.GPG( gnupghome=keydir )
    res = gpg.import_keys( key_bin )

    try:
        assert res.count == 1, "Failed to store key"
    except AssertionError, e:
        log.exception(e)
        log.error("Failed to store key")
        return None

    return res.fingerprints[0]


def gpg_remove_key( key_id, gitsec_dir=None ):
    """
    Remove a public key from our gitsec keyring
    Return True on success
    Return False on error
    """

    if gitsec_dir is None:
        gitsec_dir = gitsec_path()

    keydir = gitsec_keydir( basepath=gitsec_keydir )
    gpg = gnupg.GPG( gnugpghome=keydir )
    res = gpg.delete_keys( [key_id] )
    try:
        assert res.status == 'ok', "Failed to delete key"
    except AssertionError, e:
        log.exception(e)
        log.error("Failed to delete key '%s'" % key_id)
        return False

    return True


def gpg_download_key( key_id, key_server, basepath=None ):
    """
    Download a GNUPG key.
    Do not import it into any keyrings.
    Return the ASCII-armored key
    """

    gitsec_home = basepath
    if gitsec_home is None:
        gitsec_home = gitsec_path()

    tmpdir = gitsec_tmpdir( "download", basepath=basepath )
    gpg = gnupg.GPG( gnupghome=tmpdir )
    recvdat = gpg.recv_keys( key_server, key_id )
    fingerprint = None

    try:
        assert recvdat.count == 1
        assert len(recvdat.fingerprints) == 1
        fingerprint = recvdat.fingerprints[0]

    except AssertionError, e:
        log.exception(e)
        log.error( "Failed to fetch key '%s' from '%s'" % (key_id, key_server))
        shutil.rmtree( tmpdir )
        return None

    keydat = gpg.export_keys( [fingerprint] )
    shutil.rmtree( tmpdir )
    return keydat


def gpg_key_fingerprint( key_data, basepath=None ):
    """
    Get the key ID of a given serialized key
    Return the fingerprint on success
    Return None on error
    """
    tmpdir = gitsec_tmpdir( "key_id", basepath=basepath )
    gpg = gnupg.GPG( gnupghome=tmpdir )
    res = gpg.import_keys( key_data )

    try:
        assert res.count == 1, "Failed to import key"
        assert len(res.fingerprints) == 1, "Nonsensical GPG response: wrong number of fingerprints"
        fingerprint = res.fingerprints[0]
        shutil.rmtree(tmpdir)
        return fingerprint
    except AssertionError, e:
        log.exception(e)
        shutil.rmtree(tmpdir)
        return None 


def gpg_verify_key( key_id, key_data, basepath=None ):
    """
    Verify that a given serialized key, when imported, has the given key ID.
    Return True on success
    Return False on error
    """

    sanitized_key_id = "".join( key_id.upper().split(" ") )
    fingerprint = gpg_key_fingerprint( key_data, basepath=basepath )

    if sanitized_key_id != fingerprint and fingerprint.endswith( sanitized_key_id ):
        log.debug("Imported key does not match the given ID")
        return False

    else:
        return True


def gpg_export_key( key_id, basepath=None ):
    """
    Get the ASCII-armored key, given the ID
    """
    if basepath is None:
        basepath = gitsec_path()

    keydir = gitsec_keydir( basepath=basepath )
    gpg = gnupg.GPG( gnupghome=keydir )
    keydat = gpg.export_keys( [key_id] )
    return keydat


