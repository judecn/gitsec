#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    gitsec
    ~~~~~
    copyright: (c) 2014-2015 by Halfmoon Labs, Inc.
    copyright: (c) 2016 by Blockstack.org

    This file is part of gitsec.

    Gitsec is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gitsec is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with gitsec.  If not, see <http://www.gnu.org/licenses/>.
"""

import blockstack_client
import os
import sys
import traceback
import gnupg
import urllib2
import tempfile
import shutil
import base64
import copy
import json
import logging
from ConfigParser import SafeConfigParser

from gpg import *
from paths import *
from db import *
from git import *

from log import log

GITSEC_KEY_MAGIC = "gitsec"
GITSIC_DEFAULT_KEYSERVER = "pgp.mit.edu"

class SecurityException(Exception):
    pass


def list_blockchain_pgp_keys( blockchain_id ):
    """
    List the set of available PGP keys for a blockchain ID.
    Return a list of (key ID or fingerprint, key URL).
    If key_id[i] is None where key_url[i] is not None, then that means
    that the key ID is not known.  However, if the URL is a blockstack://
    URL, the data fetched will have already been signed by a trusted key
    (the blockchain ID's wallet key), so the key ID will not be necessary
    for authenticitiy.
    """

    key_ids = []
    key_servers = []

    # account's key listing
    key_listing = blockstack_client.client.list_pgp_keys( blockchain_id )
    if 'error' in key_listing:
        raise Exception("Blockstack error: %s" % key_listing['error'])

    for key_data in key_listing:
        key_ids.append( key_data.get('identifier', None) )
        key_servers.append( key_data.get('contentUrl', None) )

    # immutable data key listing (look for keys that start with 'gitsec:')
    immutable_listing = blockstack_client.client.list_immutable_data( blockchain_id )
    if 'error' in immutable_listing:
        raise Exception("Blockstack error: %s" % key_listing['error'])

    for (name, data_hash) in immutable_listing['data']:
        if name.startswith( GITSEC_KEY_MAGIC + ":" ):
            key_ids.append( None )
            key_servers.append( blockstack_client.make_immutable_data_url( blockchain_id, name, data_hash ) )

    # mutable data key listing (look for keys that start with 'gitsec:')
    mutable_listing = blockstack_client.client.list_mutable_data( blockchain_id )
    if 'error' in mutable_listing:
        raise Exception("Blockstack error: %s" % mutable_listing['error'])

    for (name, version) in mutable_listing['data']:
        if name.startswith( GITSEC_KEY_MAGIC + ":" ):
            key_ids.append( None )
            key_servers.append( blockstack_client.make_mutable_data_url( blockchain_id, name, version ) )
    
    return zip( key_ids, key_servers )


def fetch_pgp_key( key_url, key_id=None, basepath=None ):
    """
    Fetch a PGP public key from the given URL.
    If the URL has no scheme, then assume it's a PGP key server.
    The key is not accepted into any keyrings.

    Return the key data on success.
    Return None on error
    """

    dat = None

    if "://" in key_url:

        opener = None 
        key_data = None
        from_blockstack = False

        # handle blockstack:// URLs
        if key_url.startswith("blockstack://"):
            blockstack_opener = blockstack_client.BlockstackHandler()
            opener = urllib2.build_opener( blockstack_opener )
            from_blockstack = True

        elif key_url.startswith("http://") or key_url.startswith("https://"):
            # fetch, but at least try not to look like a bot
            opener = urllib2.build_opener()
            opener.addheaders = [('User-agent', 'Mozilla/5.0')]

        else:
            # defaults
            opener = urllib2.build_opener()

        try:
            f = opener.open( key_url )
            key_data = f.read()
            f.close()
        except Exception, e:
            traceback.print_exc()
            if key_id is not None:
                log.error("Failed to fetch key '%s' from '%s'" % (key_id, key_url))
            else:
                log.error("Failed to fetch key from '%s'" % key_url)
            return None

        # verify, if we have the ID.
        # if we don't have the key ID, then we must be fetching from blockstack 
        # (since then the data will have already been verified by the protocol, using locally-hosted trusted information)
        if not from_blockstack and key_id is None:
            log.error( "No key ID given for key located at %s" % key_url )
            return None

        if key_id is not None:
            rc = gpg_verify_key( key_id, key_data, basepath=basepath )
            if not rc:
                return None

        else:
            # get the key ID
            key_id = gpg_key_fingerprint( key_data, basepath=basepath )

        dat = key_data 

    else:
        key_server = key_url
        dat = gpg_download_key( key_id, key_server, basepath=basepath )
        assert dat is not None and len(dat) > 0, "BUG: no key data received for '%s' from '%s'" % (key_id, key_url)

    return dat


def configure( path=None, blockchain_id=None, gpg_key_id=None, gpg_key_server=None, prompt_missing=True ):
    """
    Set up a new .gitsec directory.
    Return True on success.
    Return False on bad user input.
    Abort on error.
    """

    if path is None:
        path = os.path.join( gitsec_path(), "gitsec.conf" )

    gitsec_dir = os.path.dirname(path)

    # do we have the key we need?
    if blockchain_id is None and gpg_key_id is None:
        if not prompt_missing:
            log.error( "FATAL: Insufficient key information given" )
            sys.exit(1)

        choice = None

        while choice is None:
            input_str = "How do you want to sign commits?\n"
            input_str+= "(1) Blockchain ID\n"
            input_str+= "(2) Existing GPG key\n"
            input_str+= "Selection: "

            choice = raw_input(input_str)
            choice = choice.strip()
            try:
                choice = int(choice)
            except:
                print "Invalid input '%s'" % choice
                choice = None
                continue

            if choice not in [1, 2]:
                print "Invalid input '%s'" % choice
                choice = None
                continue

        if choice == 1:
            # obtain blockchain ID, if not given
            blockchain_id = raw_input( "Please enter your Blockchain ID: " )
            blockchain_id = blockchain_id.strip()

            print "Obtaining PGP key list..."
            try:
                all_key_data = list_blockchain_pgp_keys( blockchain_id )
            except Exception, e:
                traceback.print_exc()
                print >> sys.stderr, "FATAL: failed to load PGP key list"
                sys.exit(1)

            key_data = filter( lambda kd: kd[1] is not None, all_key_data )
            if len(key_data) == 0:
                print >> sys.stderr, "FATAL: No PGP keys found.  Please add one to your blockchain ID and try again."
                sys.exit(1)

            # which ones?
            key_selection = None
            input_str = "Found the following keys:"
            for i in xrange(0, len(key_data)):
                if key_data[0] is not None:
                    input_str += "(%s) %s, from %s" % (i+1, key_data[0], key_data[1])
                else:
                    input_str += "(%s) %s" % key_data[1]

            input_str += "Key selection: " 
            
            while selection is None:
                key_selection = raw_input( input_str )
                key_selection = selection.strip()

                try:
                    key_selection = int(key_selection)
                except:
                    print "Invalid input '%s'" % key_selection
                    key_selection = None
                    continue

                if key_selection < 1 or key_selection > len(key_data)+1:
                    print "Invalid input '%s'" % key_selection
                    key_selection = None
                    continue

            key_id, key_url = key_data[key_selection-1]
            key_bin = fetch_pgp_key( key_url, key_id=key_id )
            if key_bin is None:
                print >> sys.stderr, "FATAL: Failed to fetch key '%s' from '%s'" % (key_id, key_url)
                sys.exit(1)

            # store to our gitsec keyring 
            res = gpg_store_key( key_bin )
            if res is None:
                print >> sys.stderr, "FATAL: Failed to store PGP key '%s' from '%s'" % (key_id, key_url)
                sys.exit(1)

            if key_id is not None and res != key_id:
                # we have a problem...
                print >> sys.stderr, "FATAL: key ID mismatch (%s != %s) for key %s.  Aborting for safety..." % (key_id, res, key_url)
                sys.exit(1)

            elif key_id is None:
                key_id = res

            # propagate...
            gpg_key_id = key_id

        elif choice == 2:
            # obtain key information 
            gpg_key_id = raw_input( "Please enter your GPG key ID or fingerprint: ")
            gpg_key_server = raw_input( "Please enter your GPG key server (default: pgp.mit.edu): ")
            if len(gpg_key_server.strip()) == 0:
                gpg_key_server = "pgp.mit.edu"

    parser = SafeConfigParser()

    # global section
    parser.add_section( 'gitsec' )
    parser.set('gitsec', 'gpg_key_id', gpg_key_id )

    if blockchain_id is not None:
        parser.set('gitsec', 'blockchain_id', blockchain_id )

    elif gpg_key_id is not None and gpg_key_server is not None:
        parser.set('gitsec', 'gpg_key_server', gpg_key_server )

    else:
        raise Exception("BUG: insufficient key information")

    try:
        if not os.path.exists(gitsec_dir):
            os.makedirs( gitsec_dir, 0700 )

        with open(path, "w") as f:
            parser.write(f)
            f.flush()

    except Exception, e:
        log.exception(e)
        log.error( "FATAL: Failed to write %s" % path )
        sys.exit(1)

    return True


def init( repo_name, owner_blockchain_key_url, owner_gpg_key_id, blockchain_key_urls, gpg_key_ids, repo_path=None, basepath=None, passphrase=None ):
    """
    Initialize gitsec data in a repository:
    * set the list of blockchain key URLss and gpg key IDs who may commit.
    * sign with the repo owner's key.
    * stash a copy locally, so we can check against replay attacks.

    Return True on success
    Abort on error.
    """

    if repo_path is None:
        repo_path = os.getcwd()

    if basepath is None:
        basepath = gitsec_path()

    # should be a .git directory in the same place
    if not os.path.exists( os.path.join(repo_path, ".git")) or not os.isdir( os.path.join(repo_path, ".git")):
        log.error("FATAL: '%s' is not a git repository" % repo_path)
        sys.exit(1)

    blockchain_key_urls.sort()
    gpg_key_ids.sort()

    gitsec_data = make_gitsec_data( repo_name=repo_name, owner_blockchain_key_url=owner_blockchain_key_url, owner_gpg_key_id=owner_gpg_key_id, blockchain_key_urls=blockchain_key_urls, gpg_key_ids=gpg_key_ids )
    gitsec_path = os.path.join( repo_path, ".gitsec" )

    try:
        put_gitsec_data( repo_name, owner_gpg_key_id, gitsec_data, gitsec_path, basepath=basepath, passphrase=passphrase )
    except:
        traceback.print_exc()
        log.error("FATAL: failed to set up %s" % gitsec_path)
        sys.exit(1)

    return True


def repo_import( repo_name, blockchain_key_url=None, gpg_key_id=None, basepath=None, repo_path=None, passphrase=None ):
    """
    Import signed repo data from a repository,
    and cache a copy locally.

    This is used when you're the owner of the repository, but you're
    cloning or pulling on a host that doesn't have your ~/.gitsec directory.

    Return True on success
    Raise on error
    """
    
    assert blockchain_key_url is not None or gpg_key_id is not None, "Missing arguments: need a blockchain key URL or a GPG key ID"

    if repo_path is None:
        repo_path = os.getcwd()

    if basepath is None:
        basepath = gitsec_path()

    # should be a .git directory in the same place
    if not os.path.exists( os.path.join(repo_path, ".git")) or not os.isdir( os.path.join(repo_path, ".git")):
        raise Exception("'%s' is not a git repository" % repo_path)

    # get GPG key ID
    if gpg_key_id is None:
        # fetch from blockstack 
        key_data = fetch_pgp_key( blockchain_key_url, basepath=basepath )
        assert key_data is not None, "Failed to load key data for key '%s'" % blockchain_key_url 

        gpg_key_id = gpg_key_fingerprint( key_data, basepath=basepath )
        if gpg_key_id is None:
            raise Exception("Failed to load key from '%s'" % blockchain_key_url )

    gitsec_data = get_gitsec_data( gpg_key_id, repo_path, repo_import=True )
    assert gitsec_data is not None, "Failed to load .gitsec data from '%s'" % repo_path

    repo_data_store( repo_name, json.dumps(gitsec_data), basepath )
    return True


def download_pgp_keys( gitsec_data, basepath=None, partial=False ):
    """
    Download keys from gitsec data.  Cache downloaded data in directory @basepath.
    Return a list of {'key': keydata, 'id': url_or_id} on success
    Raise on download error, unless @partial is True (in which case, missing keys will be mapped to None)
    """

    blockchain_key_urls = gitsec_data.get('blockchain_key_urls', [])
    gpg_key_ids = gitsec_data.get('gpg_key_ids', [])

    blockchain_keys = {}    # map URL to key data
    gpg_keys = {}           # map key ID to key data

    for blockchain_key_url in blockchain_key_urls:

        key_data = fetch_pgp_key( key_url, basepath=basepath )
        if not partial:
            assert key_data is not None, "Failed to load key data for key %s" % blockchain_key_url

        blockchain_keys[key_url] = key_data

    for gpg_key_id in gpg_key_ids:
        
        key_data = fetch_pgp_key( GITSEC_DEFAULT_SERVER, key_id=owner_gpg_key_id, basepath=basepath )
        if not partial:
            assert key_data is not None, "failed to load key data for key %s" % gpg_key_id

        gpg_keys[key_id] = key_data

    return [ {'key': bc[url], 'id': url} for url in blockchain_keys.keys() ] + \
           [ {'key': gk[kid], 'id': kid} for kid in gpg_keys.keys() ]


def get_owner_gpg_key_id( repo_name=None, owner_blockstack_key_url=None, basepath=None ):
    """
    Fetch and validate the repo owner's GPG key ID.
    Either the blockstack key URL is known, or we have already fetched the signed repo data and we 
    know the repo name.
    Return the GPG key ID on success
    Raise on error
    """

    owner_gpg_key_id = None

    if owner_blockstack_key_url is None:
        # look up from local ~/.gitsec 
        assert repo_name is None, "Need either repo owner's GPG key ID or repo name"
        repo_data = repo_data_load( repo_name, basepath )
        if repo_data is None:
            raise SecurityException("Insufficient information to verify gitsec authenticity")

        owner_gpg_key_id = repo_data.get('owner_gpg_key_id', None)
        owner_blockstack_key_url = repo_data.get('owner_blockchain_key_url', None)

        if owner_gpg_key_id is None and owner_blockstack_url is None:
            raise SecurityException("Missing both blockstack URL and GPG key")

    if owner_gpg_key_id is None:
        # look up via blockstack
        key_data = fetch_pgp_key( owner_blockstack_key_url )
        if key_data is None:
            raise SecurityException("Unable to obtain owner's public key")

        key_id = gpg_key_fingerprint( key_data )
        if key_id is None:
            raise SecurityException("Unable to obtain owner's public key fingerprint")

        owner_gpg_key_id = key_id

    return owner_gpg_key_id


def verify_head( owner_gpg_key_id=None, owner_blockstack_key_url=None, repo_name=None, repo_path=None, basepath=None ):
    """
    Verify that the current HEAD is signed by one of the
    keys in the current .gitsec data.

    Return True if so.
    Return False if not.
    """

    if repo_path is None:
        repo_path = os.getcwd()

    if basepath is None:
        basepath = gitsec_path()

    if owner_gpg_key_id is None:
        owner_gpg_key_id = get_owner_gpg_key_id( repo_name=repo_name, owner_blockstack_key_url=owner_blockstack_key_url, basepath=basepath )
        assert owner_gpg_key_id is not None, "BUG: failed to resolve owner's GPG key ID"

    gitsec_data_path = os.path.join(repo_path, ".gitsec")

    # load gitsec data... 
    gitsec_data = get_gitsec_data( gitsec_data_path, basepath=basepath )

    # fetch the rest of the keys
    key_data = download_pgp_keys( gitsec_data, basepath=basepath, partial=True )

    # temporarily stash them 
    # TODO... 

    """
    log_head = git_log( repo_path=repo_path, basepath=basepath, depth=1 )
    if log_head[0]['sig_status'] != 'G':


    pass
    """
