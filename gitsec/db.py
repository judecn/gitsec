#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    gitsec
    ~~~~~
    copyright: (c) 2014-2015 by Halfmoon Labs, Inc.
    copyright: (c) 2016 by Blockstack.org

    This file is part of gitsec.

    Gitsec is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gitsec is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with gitsec.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import json
import gnupg
import copy

from log import log 
from paths import *
from git import *

def repo_data_store( repo_name, data, basepath ):
    """
    Locally store the signed repo name and data, so we can compare against
    the data in the repository.
    """
    repo_data_path = os.path.join( basepath, "%s.json" % repo_name )
    with open( repo_data_path, "w" ) as f:
        f.write( data )
        f.flush()
        os.fsync(f.fileno())

    return True


def repo_data_load( repo_name, basepath ):
    """
    Load locally-stored signed repository information, so we can
    compare against the data in the repository itself.
    Returns a dict with the repo information on success.
    Returns None on error.

    NOTE: the signature will NOT be checked.
    """
    repo_data_path = os.path.join( basepath, "%s.json" % repo_name )
    repo_data = None

    if not os.path.exists( repo_data_path ):
        log.warning("No such file or directory: %s" % repo_data_path )
        return None

    with open( repo_data_path, "r") as f:
        repo_data = f.read()

    try:
        repo_info = json.loads(repo_data)
        return repo_info
    except:
        log.error("Failed to parse '%s' as JSON" % repo_data_path )
        return None


def put_gitsec_data( repo_name, owner_gpg_key_id, data, path, basepath=None, passphrase=None ):
    """
    Store signed .gitsec data to the given path
    Return True on success
    Raise on error
    """

    gitsec_data = copy.deepcopy( data )

    if basepath is None:
        basepath = gitsec_path()

    gitsec_data_str = json.dumps(gitsec_data, sort_keys=True)
    keydir = gitsec_keydir( basepath )

    # sign with owner 
    gpg = gnupg.GPG( gnupghome=keydir )
    sigres = gpg.sign( gitsec_data_str, keyid=owner_gpg_key_id, passphrase=passphrase, detach=True )
    assert sigres, "Failed to sign gitsec data"
    assert len(sigres.data) > 0, "Failed to sign gitsec data"

    # re-serialize 
    gitsec_data['sig'] = sigres.data
    gitsec_data_str = json.dumps(gitsec_data, sort_keys=True )
    
    # store signed data
    with open( path, "w" ) as f:
        f.write( gitsec_data_str )
        f.flush()
        os.fsync(f.fileno())
    
    # remember the owner of this repo, and the repo name 
    repo_data_store( repo_name, gitsec_data_str, basepath )

    return True


def get_gitsec_data( owner_gpg_key_id, path, basepath=None, repo_import=False ):
    """
    Read and verify .gitsec data in the given path.
    Only do this if all the keys are sync'ed to @basepath.

    If @repo_import is False, then we're expected to have a copy of the
    signed repo information in ~/.gitsec already (override with @basepath).
    Otherwise, the data will be copied to ~/.gitsec if it was signed by the owner
    gpg key.

    Return True on success
    Raise on error
    """

    if basepath is None:
        basepath = gitsec_path()

    keydir = gitsec_keydir( basepath )

    data_txt = None
    with open( path, "r" ) as f:
        data_txt = f.read()

    # verify with keyring
    gitsec_data = json.loads( data_txt )
    assert 'sig' in gitsec_data, "Missing signature"
    
    gitsec_sig = gitsec_data['sig']
    del gitsec_data['sig']

    # temporarily stash the signature file
    tmpsig_fd, tmpsig_path = gitsec_tmpfile( "verify-sig", basepath=basepath )
    tmpsig_fh = os.fdopen( tmpsig_fd, "w" )
    tmpsig_fh.write( gitsec_sig )
    tmpsig_fh.flush()
    tmpsig_fh.close()

    # verify the signature against the owner's key (which must be on-file already)
    verify_txt = json.dumps(gitsec_data, sort_keys=True)
    gpg = gnupg.GPG( gnupghome=keydir )
    vres = gpg.verify_data( tmpsig_path, verify_txt )

    try:
        os.unlink(tmpsig_path)
    except:
        pass

    try:
        assert vres.valid, "Failed to verify gitsec data"
        assert vres.fingerprint.endswith( owner_gpg_key_id ), "Not signed by key '%s'" % owner_gpg_key_id
    except AssertionError:
        raise SecurityException("Repo data signature mismatch")

    # verify that the last commit to the .gitsec data was signed by the owner 
    gitsec_commits = git_log( basepath=basepath, repo_path=os.path.dirname(path), depth=2, file_path=".gitsec" ) 
    if len(gitsec_commits) > 0:
        if gitsec_commits[0]['sig_status'] != 'G':
            # not signed
            raise SecurityException("Unsigned .gitsec data")
        
        if not owner_gpg_key_id.endswith(gitsec_commits[0]['signing_key_id']):
            # signed by someone else
            raise SecurityException(".gitsec data signed by non-owner key %s" % git_commits[0]['signing_key_id'])

    if not repo_import:
        # verify that the .gitsec data matches the locally-hosted repo data 
        assert gitsec_data.has_key('repo_data'), "Gitsec repo data is missing repository name"

        local_repo_data = repo_data_load( gitsec_data['repo_name'], basepath )
        if local_repo_data is None:
            # we cannot trust this data 
            raise SecurityException("Cannot verify authenticity of repo data")

        # must match data in the repo
        gitsec_data['sig'] = gitsec_sig
        if gitsec_data != local_repo_data:
            # we didn't put repo data 
            raise SecurityException("Local .gitsec data does not match repository .gitsec data.")

    # success!
    return gitsec_data


def make_gitsec_data( repo_name=None, owner_blockchain_key_url=None, owner_gpg_key_id=None, blockchain_key_urls=None, gpg_key_ids=None ):
    """
    Create gitsec data to sign
    """
    ret = {}

    if repo_name is not None:
        ret['repo_name'] = repo_name

    if owner_blockchain_key_url is not None:
        ret['owner_blockchain_key_url'] = owner_blockchain_key_url

    if owner_gpg_key_id is not None:
        ret['owner_gpg_key_id'] = owner_gpg_key_id

    if blockchain_key_urls is not None:
        ret['blockchain_key_urls'] = blockchain_key_urls

    if gpg_key_ids is not None:
        ret['gpg_key_ids'] = gpg_key_ids

    return ret


