#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
    gitsec
    ~~~~~
    copyright: (c) 2014-2015 by Halfmoon Labs, Inc.
    copyright: (c) 2016 by Blockstack.org

    This file is part of gitsec.

    Gitsec is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gitsec is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with gitsec.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import sys
import tempfile

def gitsec_path():
    """
    Get the path to the default .gitsec dir
    """

    gitsec_dir = os.expanduser("~/.gitsec")
    return gitsec_dir


def gitsec_keydir( basepath=None ):
    """
    Make a path to the gitsec keyring
    """
    if basepath is None:
        basepath = gitsec_path()

    return os.path.join( basepath, "keyrings" )


def gitsec_tmpdir( prefix, basepath=None ):
    """
    Make a temporary directory in the .gitsec directory
    """
    if basepath is None:
        basepath = gitsec_path()

    tmppath = os.path.join( basepath, "tmp" )
    if not os.path.exists(tmppath):
        os.makedirs(tmppath, 0700 )

    tmpdir = tempfile.mkdtemp( prefix=('%s-' % prefix), dir=tmppath )
    return tmpdir


def gitsec_tmpfile( prefix, basepath=None ):
    """
    Make a temporary directory in the .gitsec directory
    """
    if basepath is None:
        basepath = gitsec_path()

    tmppath = os.path.join( basepath, "tmp" )
    if not os.path.exists(tmppath):
        os.makedirs(tmppath, 0700 )

    tmpfile_fd, tmpfile_path = tempfile.mkstemp( prefix=('%s-' % prefix), dir=tmppath )
    return tmpfile_fd, tmpfile_path

