Gitsec
======

Gitsec is a tool for managing a git repository's developers' GPG keys.  Gitsec keeps the set of keys synchronized with the developers' [blockchain IDs](https://blockstack.org) or key servers.  It provides a simple interface for adding and removing keys, and signing and verifying commits and tags.
